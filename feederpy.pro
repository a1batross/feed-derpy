TEMPLATE = app

QT += qml quick

SOURCES += main.cpp

RESOURCES += res.qrc

# Default rules for deployment.
include(deployment.pri)
