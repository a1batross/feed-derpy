#include <QGuiApplication>
#include <QQuickView>

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    QQuickView engine(QUrl(QStringLiteral("qrc:///qml/main.qml")));

    engine.setResizeMode(QQuickView::SizeRootObjectToView);
    engine.show();
    engine.setTitle(QStringLiteral("Feed Derpy!"));

    return app.exec();
}
