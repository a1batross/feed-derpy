import QtQuick 2.2

Item {
    id: item
    property bool eating: false

    property int mouth: animation.width * 0.43
    property int mouthY: item.y + animation.height * 0.2
    property int hairY: item.y + animation.height * 0.05

    function eat(){ animation.start(); }

    onEatingChanged: { if(!eating) screen.eated++ }

    AnimatedSprite {
        id: animation
        running: false
        loops: 1
        width: 300
        height: 232
        onRunningChanged: { if(!running) parent.eating = false }
        frameCount: 10
        frameRate: 11
        frameWidth: 600
        frameHeight: 464
        interpolate: false
        source: "/images/sprite.png"
    }
}
