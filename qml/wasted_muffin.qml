import QtQuick 2.0
import "library.js" as Library

Image {
    id: wasted_muffin
    source: "/images/muffin.png"
    x: (screen.width / 2 ) - wasted_muffin.width / 2
    y: 150
    height: 43
    width: 50

    property int plusOrMinus: Library.randomNegaPositive()

    SequentialAnimation {
        id: fallingImage
        running: true
        onRunningChanged: {
            if(!running) {
                screen.wasted++;
                wasted_muffin.destroy();
                gc();
            }
        }
        NumberAnimation {
            target: wasted_muffin
            property: "y"
            to: derpy.hairY
            duration: 500
        }
        ParallelAnimation {
            PathAnimation {
            target: wasted_muffin
            duration: 500
            orientation: PathAnimation.RightFirst
            anchorPoint: Qt.point(wasted_muffin.width/2, wasted_muffin.height/2)
            path: Path {
                PathCurve { relativeX: plusOrMinus * 25; relativeY: -30 }
                PathCurve { relativeX: plusOrMinus * 25; relativeY: 60 }
                }
            }
            NumberAnimation {
                target: wasted_muffin
                duration: 500
                property: "opacity"
                from: 1
                to: 0
            }
        }
    }
}

