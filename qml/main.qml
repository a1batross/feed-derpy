import QtQuick 2.2
import QtQuick.Window 2.1
import "library.js" as Library


Rectangle {
    id: screen
    width: 480
    height: 720
    visible: true
    color: "aliceblue"

    property int eated: 0
    property int wasted: 0

    onEatedChanged: { eated_text.text = "Muffins Eated: " + eated; }
    onWastedChanged: { wasted_text.text = "Muffins Wasted: " + wasted; }

    Rectangle  {
        id: header
        width: parent.width
        height: 144
        color: "black"
        anchors.top: screen.top
        Image {
            source: "/images/logo.png"
            anchors.centerIn: parent
        }
        Text {
            id: eated_text
            text: "Muffins Eated: 0"
            color: "white"
            font.pixelSize: 24
            anchors { top: parent.top; left: parent.left; }
        }
        Text {
            id: wasted_text
            text: "Muffins Wasted: 0"
            color: "white"
            font.pixelSize: 24
            anchors { top: parent.top; right: parent.right;  }
        }
    }

    Image  {
        id: cloud
        fillMode: Image.PreserveAspectFit
        anchors { bottom: screen.bottom; horizontalCenter: screen.horizontalCenter}
        height: 200
        width: 300
        source: "/images/cloud.png"
    }

    Image {
        id: basket
        source: "/images/basket.png"
        x: screen.width / 2 - basket.width / 2
        y: header.height - 50
        MouseArea {
            width: parent.width; height: parent.height
            onClicked: {  Library.createMuffinObject()  }
        }
    }

    // Just a derpy ^_^
    Derpy {
        id: derpy
        x: (screen.width / 2 ) - derpy.mouth
        y: cloud.y - cloud.height / 2
    }
}
