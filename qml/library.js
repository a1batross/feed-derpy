var component;
var sprite;

function createMuffinObject() {
    if(derpy.eating) {
        component = Qt.createComponent("wasted_muffin.qml");
    } else {
        derpy.eating = true;
        component = Qt.createComponent("muffin.qml");
    }

    if (component.status == Component.Ready)
        finishCreation();
    else
        component.statusChanged.connect(finishCreation);
}

function finishCreation() {
    if (component.status == Component.Ready) {
        sprite = component.createObject(screen);
        if (sprite == null) {
            console.log("Error creating object");
        }
    } else if (component.status == Component.Error) {
        console.log("Error loading component:", component.errorString());
    }
}

function randomNegaPositive() {
    return Math.random() < 0.5 ? -1 : 1;
}
