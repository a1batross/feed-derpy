import QtQuick 2.0

Image {
    id: muffin
    source: "/images/muffin.png"
    x: (screen.width / 2 ) - muffin.width / 2
    y: 150
    height: 43
    width: 50

    SequentialAnimation {
        id: fallingImage
        running: true
        onRunningChanged: {
            if(!running) {
                derpy.eat();
                muffin.destroy();
                gc();
            }
        }
        NumberAnimation {
            target: muffin
            property: "y"
            to: derpy.mouthY
            duration: 500
        }
    }
}
